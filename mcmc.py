import pymc3 as pm
import scipy.stats as st 
import pandas as pd
import numpy as np
from functools import partial
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from numpy.random import gamma as rgamma # rename so we can use gamma for parameter name

plt.ion()
np.random.seed(1234)

# coin toss analytical solution
n = 100
h = 61
p = h/n 
rv = st.binom(n,p)
mu = rv.mean()

a,b = 10,10
prior = st.beta(a,b)
post = st.beta(h+a,n-h+b)
ci = post.interval(0.95)
thetas = np.linspace(0,1,200)

# plot
plt.figure(figsize=(12,9))
plt.style.use('ggplot')
plt.plot(thetas,prior.pdf(thetas),label='Prior',c='blue')
plt.plot(thetas,post.pdf(thetas),label='Posterior',c='red')
plt.plot(thetas,n*st.binom(n,thetas).pmf(h),label='Likelihood',c='green')
plt.axvline((h+a-1)/(n+a+b-2),c='red',linestyle='dashed',alpha=0.4,label='MAP')
plt.axvline(mu/n,c='green',linestyle='dashed',alpha=0.4,label='MLE')
plt.xlim([0,1])
plt.axhline(0.3,ci[0],ci[1],c='black',linewidth=2,label='95% CI');
plt.xlabel(r'$\theta$',fontsize=14)
plt.ylabel('Density',fontsize=16)
plt.legend();

# numerical integration
thetas = np.linspace(0,1,200)
prior = st.beta(a,b)
post = prior.pdf(thetas)*st.binom(n,thetas).pmf(h)
post /= (post.sum() / len(thetas))

plt.figure(figsize=(12,9))
plt.plot(thetas, prior.pdf(thetas), label='Prior', c='blue')
plt.plot(thetas, n*st.binom(n, thetas).pmf(h), label='Likelihood', c='green')
plt.plot(thetas, post, label='Posterior', c='red')
plt.xlim([0, 1])
plt.xlabel(r'$\theta$', fontsize=14)
plt.ylabel('Density', fontsize=16)
plt.legend();

# metropolis-hastings 
# proposal Gaussian(0,sigma^2)
# target proportional to posterior -> L*prior
# theta <- theta + rnd(proposal)

def target(lik,prior,n,h,theta):
    if theta < 0 or theta > 1:
        return 0
    else:
        return lik(n,theta).pmf(h)*prior.pdf(theta)

def mh_coin(niters,n,h,theta,lik,prior,sigma):
    samples = [theta]
    while len(samples) < niters:
        theta_p = theta + st.norm(0,sigma).rvs()
        rho = min(1,target(lik,prior,n,h,theta_p)/target(lik,prior,n,h,theta))
        u = np.random.uniform()
        if u < rho:
            theta = theta_p
        samples.append(theta)
    return samples

n = 100
h = 61
a = 10
b = 10
lik = st.binom
prior = st.beta(a,b)
post = st.beta(h+a, n-h+b)
sigma = 0.05 # 0.3
thetas = np.linspace(0, 1, 200)
naccept = 0
theta = 0.1
niters = 100 # 10000
samples = np.zeros(niters+1)
samples[0] = theta
for i in range(niters):
    theta_p = theta + st.norm(0,sigma).rvs()
    rho = min(1,target(lik,prior,n,h,theta_p) / target(lik,prior,n,h,theta))
    u = np.random.uniform()
    if u < rho: 
        naccept += 1
        theta = theta_p
    samples[i+1] = theta
nmcmc = len(samples)//2
print('Efficiency = ',naccept/niters)
# plot
plt.figure(figsize=(12, 9))
plt.hist(samples[nmcmc:], 40, histtype='step', normed=True, linewidth=1,
        label='Distribution of prior samples');
plt.hist(prior.rvs(nmcmc), 40, histtype='step', normed=True, linewidth=1,
        label='Distribution of posterior samples');
plt.plot(thetas, post.pdf(thetas), c='red', linestyle='--', alpha=0.5,
        label='True posterior')
plt.xlim([0,1]);
plt.legend(loc='best');

# convergence of multiple chains
sampless = [mh_coin(niters, n, h, theta, lik, prior, sigma) for theta in np.arange(0.1, 1, 0.2)]
for samples in sampless:
    plt.plot(samples,'-o')
plt.xlim([0,niters])
plt.ylim([0,1])

# Gibbs sampler
def bern(theta,z,N):
    '''Bernoulli likelihood with N trials and z successes'''
    return np.clip(theta**z*(1-theta)**(N-z),0,1)

def bern2(theta1,theta2,z1,z2,N1,N2):
    '''Bernoulli likelihood with N trials and z successes'''
    return bern(theta1,z1,N1) * bern(theta2,z2,N2)

def make_thetas(xmin,xmax,n):
    xs = np.linspace(xmin,xmax,n)
    widths = (xs[1:] - xs[:-1])/2.0
    thetas = xs[:-1] + widths
    return thetas

def make_plots(X,Y,prior,likelihood,posterior,projection=None):
        fig, ax = plt.subplots(1,3, subplot_kw=dict(projection=projection,
        aspect='equal'), figsize=(12,3))
        if projection == '3d':
            ax[0].plot_surface(X, Y, prior, alpha=0.3,
            cmap=plt.cm.jet)
            ax[1].plot_surface(X, Y, likelihood, alpha=0.3,
            cmap=plt.cm.jet)
            ax[2].plot_surface(X, Y, posterior,
            alpha=0.3, cmap=plt.cm.jet)
        else:
            ax[0].contour(X,Y,prior)
            ax[1].contour(X,Y,likelihood)
            ax[2].contour(X,Y,posterior)
        ax[0].set_title('Prior')
        ax[1].set_title('Likelihood')
        ax[2].set_title('Posterior')
        plt.tight_layout()

thetas1 = make_thetas(0,1,101)
thetas2 = make_thetas(0,1,101)
X,Y = np.meshgrid(thetas1,thetas2)

# analytic solution
a = 2
b = 3
z1 = 11
N1 = 14
z2 = 7
N2 = 14
prior = st.beta(a,b).pdf(X) * st.beta(a,b).pdf(Y)
likelihood = bern2(X,Y,z1,z2,N1,N2)
posterior = st.beta(a + z1,b+N1-z1).pdf(X) * st.beta(a+z2,b+N2-z2).pdf(Y)
# plot 
make_plots(X,Y,prior,likelihood,posterior)
make_plots(X,Y,prior,likelihood,posterior,projection='3d')

# grid approximation
def c2d(thetas1,thetas2,pdf):
    width1 = thetas1[1] - thetas1[0]
    width2 = thetas2[1] - thetas2[0]
    area = width1*width2
    pmf = pdf*area
    pmf /= pmf.sum()
    return pmf

_prior = bern2(X, Y, 2, 8, 10, 10) + bern2(X, Y, 8, 2, 10, 10)
prior_grid = c2d(thetas1, thetas2, _prior)
_likelihood = bern2(X, Y, 1, 1, 2, 3)
posterior_grid = _likelihood * prior_grid
posterior_grid /= posterior_grid.sum()
make_plots(X, Y, prior_grid, likelihood, posterior_grid)
make_plots(X, Y, prior_grid, likelihood, posterior_grid, projection='3d')

# gibbs
a = 2
b = 3

z1 = 11
N1 = 14
z2 = 7
N2 = 14
prior = lambda theta1, theta2: st.beta(a, b).pdf(theta1) * st.beta(a,
        b).pdf(theta2)
lik = partial(bern2, z1=z1, z2=z2, N1=N1, N2=N2)
target = lambda theta1, theta2: prior(theta1, theta2) * lik(theta1, theta2)

theta = np.array([0.5, 0.5])
niters = 10000
burnin = 500
sigma = np.diag([0.2,0.2])

thetas = np.zeros((niters-burnin, 2), np.float)
for i in range(niters):
    theta = [st.beta(a + z1, b + N1 - z1).rvs(), theta[1]]
    theta = [theta[0], st.beta(a + z2, b + N2 - z2).rvs()]

    if i >= burnin:
        thetas[i-burnin] = theta

kde = st.gaussian_kde(thetas.T)
XY = np.vstack([X.ravel(), Y.ravel()])
posterior_gibbs = kde(XY).reshape(X.shape)
make_plots(X, Y, prior(X, Y), lik(X, Y), posterior_gibbs)
make_plots(X, Y, prior(X, Y), lik(X, Y), posterior_gibbs, projection='3d')

# Slice sampling
# simple example from Gaussian
dist = st.norm(5, 3)
w = 0.5
x = dist.rvs()

niters = 1000
xs = []
while len(xs) < niters:
    y = np.random.uniform(0, dist.pdf(x))
    lb = x
    rb = x
    while y < dist.pdf(lb):
        lb -= w
    while y < dist.pdf(rb):
        rb += w
    x = np.random.uniform(lb, rb)
    if y > dist.pdf(x):
        if np.abs(x-lb) < np.abs(x-rb):
            lb = x
        else:
            lb = y
    else:
        xs.append(x)

plt.hist(xs, 20);

# hierarchical modeling
def lambda_update(alpha, beta, y, t):
    return rgamma(size=len(y), shape=y+alpha, scale=1.0/(t+beta))

def beta_update(alpha, gamma, delta, lambd, y):
    return rgamma(size=1, shape=len(y) * alpha + gamma, scale=1.0/(delta + lambd.sum()))

def gibbs(niter, y, t, alpha, gamma, delta):
    lambdas_ = np.zeros((niter, len(y)), np.float)
    betas_ = np.zeros(niter, np.float)

    lambda_ = y/t

    for i in range(niter):
        beta_ = beta_update(alpha, gamma, delta, lambda_, y)
        lambda_ = lambda_update(alpha, beta_, y, t)

        betas_[i] = beta_
        lambdas_[i,:] = lambda_

    return betas_, lambdas_

alpha = 1.8
gamma = 0.01
delta = 1.0
beta0 = 1
y = np.array([5, 1, 5, 14, 3, 19, 1, 1, 4, 22], np.int)
t = np.array([94.32, 15.72, 62.88, 125.76, 5.24, 31.44, 1.05, 1.05, 2.10, 10.48], np.float)
niter = 1000
betas, lambdas = gibbs(niter, y, t, alpha, gamma, delta)
print('%.3f' % betas.mean())
print('%.3f' % betas.std(ddof=1))
print(lambdas.mean(axis=0))
print(lambdas.std(ddof=1, axis=0))
# plot
plt.figure(figsize=(10, 20))
for i in range(len(lambdas.T)):
    plt.subplot(5,2,i+1)
    plt.plot(lambdas[::10, i]);
    plt.title('Trace for $\lambda$%d' % i)
