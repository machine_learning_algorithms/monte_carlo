Scripts for implementing Markov-Chain Monte Carlo and Hamiltonian Monte Carlo. Includes examples for Metropolis-Hastings and Gibbs sampling.
